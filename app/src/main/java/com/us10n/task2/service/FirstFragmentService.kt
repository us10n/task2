package com.us10n.task2.service

import com.us10n.task2.recycler.RecyclerViewItem

object FirstFragmentService {
    fun createRecyclerList(): ArrayList<RecyclerViewItem> {
        val recyclerItems = ArrayList<RecyclerViewItem>();
        for (i in 1..1000) {
            recyclerItems.add(RecyclerViewItem("item_image${i}", "Title $i", "Description $i"))
        }
        return recyclerItems
    }
}