package com.us10n.task2.fragment

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.transition.TransitionInflater
import com.us10n.task2.databinding.FragmentSecondBinding

class SecondFragment : Fragment() {

    private lateinit var binding: FragmentSecondBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            sharedElementEnterTransition =
                TransitionInflater.from(requireContext()).inflateTransition(android.R.transition.move)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSecondBinding.inflate(inflater)

        binding.toolBar.setNavigationOnClickListener {
            returnFirstFragment()
        }
        binding.closeButton.setOnClickListener {
            closeApp()
        }
        arguments?.let {
            val args: SecondFragmentArgs by navArgs()
            binding.title.text = args.title
            binding.description.text = args.description
        }

        return binding.root
    }


    private fun returnFirstFragment() {
        findNavController().popBackStack()
    }


    private fun closeApp() {
        activity?.finishAffinity()
    }
}