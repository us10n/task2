package com.us10n.task2.fragment

import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.Navigation
import androidx.navigation.fragment.FragmentNavigator
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.us10n.task2.R
import com.us10n.task2.RecyclerViewItemListener
import com.us10n.task2.databinding.FragmentFirstBinding
import com.us10n.task2.recycler.RecyclerViewAdapter
import com.us10n.task2.recycler.RecyclerViewItem
import com.us10n.task2.service.FirstFragmentService

class FirstFragment : Fragment(), RecyclerViewItemListener {

    private lateinit var binding: FragmentFirstBinding
    private val recyclerAdapter = RecyclerViewAdapter(this)

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFirstBinding.inflate(inflater)

        binding.recycler.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@FirstFragment.context)
            adapter = recyclerAdapter
        }
        recyclerAdapter.items = FirstFragmentService.createRecyclerList()

        return binding.root
    }

    override fun onClick(recyclerItem: RecyclerViewItem, imageView: ImageView) {
        openSecondFragment(recyclerItem, imageView)
    }

    private fun openSecondFragment(recyclerItem: RecyclerViewItem, imageView: ImageView) {
        val action = FirstFragmentDirections.actionFirstFragmentToSecondFragment().apply {
            this.title = recyclerItem.title
            this.description = recyclerItem.description
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val extras = FragmentNavigatorExtras(imageView to "big_image")
            findNavController().navigate(action, extras)
        } else {
            findNavController().navigate(action)
        }
    }
}