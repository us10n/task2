package com.us10n.task2.recycler

import android.media.Image
import android.os.Build
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.us10n.task2.R
import com.us10n.task2.RecyclerViewItemListener
import com.us10n.task2.databinding.RecyclerItemBinding

class RecyclerViewAdapter(private val listener: RecyclerViewItemListener) :
    RecyclerView.Adapter<RecyclerViewHolder>() {
    var items = listOf<RecyclerViewItem>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = RecyclerItemBinding.inflate(layoutInflater, parent, false);
        return RecyclerViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerViewHolder, position: Int) {
        holder.itemView.setOnClickListener {
            val imageView = it.findViewById<ImageView>(R.id.itemImageView)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                imageView.transitionName = "item_image${position}"
            }
            listener.onClick(
                items[position],
                imageView
            )
        }
        holder.bind(items[position])
    }

    override fun getItemCount(): Int = items.size
}