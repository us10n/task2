package com.us10n.task2.recycler

import android.os.Build
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.us10n.task2.databinding.RecyclerItemBinding

class RecyclerViewHolder(private val binding: RecyclerItemBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun bind(recyclerItem: RecyclerViewItem) {
        binding.title.text = recyclerItem.title
        binding.description.text = recyclerItem.description
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            binding.itemImageView.transitionName="item_image${recyclerItem.id}"
        }
    }
}