package com.us10n.task2.recycler

data class RecyclerViewItem(
    val id:String,
    val title: String,
    val description: String
) {
}