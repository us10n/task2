package com.us10n.task2

import android.widget.ImageView
import com.us10n.task2.recycler.RecyclerViewItem

interface RecyclerViewItemListener {
    fun onClick(item: RecyclerViewItem, imageView: ImageView)
}